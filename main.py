from prompt_toolkit import prompt
from prompt_toolkit.formatted_text import ANSI, HTML
from prompt_toolkit.styles import Style
import os
import sys
from datetime import datetime

from astropy.coordinates import SkyCoord, EarthLocation, AltAz
import astropy.units as u
from astropy.time import Time

style = Style.from_dict({
    'bottom-toolbar': '#000000',
})

white = "\033[48;5;7m"
green = "\033[48;5;2m"

location = EarthLocation.of_address(sys.argv[1])
time = Time(datetime.utcnow())

os.system("clear")
while True:
    text = prompt('> ', bottom_toolbar=ANSI('{}Connected{} | Other info'.format(green, white)), style=style)

    if text == "exit":
        print ("Goodbye!")
        sys.exit()

    object = SkyCoord.from_name(text)
    print (object.ra, object.dec, object.transform_to(AltAz(obstime=time,location=location)).alt)
